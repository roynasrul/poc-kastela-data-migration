package datamigration

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"math"
	"runtime"
	"time"

	"github.com/alitto/pond"
	"github.com/goombaio/namegenerator"
	uuid "github.com/satori/go.uuid"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name            string
	No_id           string
	Encrypted_no_id string
}

type Temp struct {
	gorm.Model
	Id              uint
	Encrypted_no_id string
	Code            uint
}

func dataMigration() {
	runtime.GOMAXPROCS(2)
	// done := make(chan bool)

	// // database connection
	dsn := "host=localhost user=postgres password=R00Tpostgres dbname=kastela port=5432 sslmode=disable TimeZone=Asia/Jakarta"
	db, err := gorm.Open(postgres.New(postgres.Config{DSN: dsn, PreferSimpleProtocol: true}), &gorm.Config{})
	errorHandler(err)

	// migrate db
	migrateTemp(db) // digunakan juga untuk mengosongkan table temporary
	migrate(db)

	// generator nama untuk seeder
	nameSeed := time.Now().UTC().UnixNano()
	nameGenerator := namegenerator.NewNameGenerator(nameSeed)
	seed(db, nameGenerator)

	pondProcess(db, 10, 50)

	// <-done
}

func migrate(db *gorm.DB) {
	db.Migrator().DropTable(&User{})
	db.AutoMigrate(&User{})
}

func migrateTemp(db *gorm.DB) {
	db.Migrator().DropTable(&Temp{})
	db.AutoMigrate(&Temp{})
}

func seed(db *gorm.DB, nameGenerator namegenerator.Generator) {
	for i := 0; i < 10000; i++ {
		db.Create(&User{Name: nameGenerator.Generate(), No_id: uuid.NewV4().String()})
		fmt.Println("seed :", i+1)
	}
}

func aesgcm(text string, key []byte) (cipherText []byte) {
	c, err := aes.NewCipher(key)
	errorHandler(err)

	gcm, err := cipher.NewGCM(c)
	errorHandler(err)

	nonce := make([]byte, gcm.NonceSize())
	_, err = rand.Read(nonce)
	errorHandler(err)

	cipherText = gcm.Seal(nonce, nonce, []byte(text), nil)
	return
}

func encrypt(i int, db *gorm.DB, key []byte, shardSize int) {
	var offset int64
	if i == 0 {
		offset = 0
	} else {
		offset = int64(i) * int64(shardSize)
	}

	var users []User
	// db.Order("id").Limit(shardSize).Offset(int(offset)).Find(&users)
	db.Order("id").Where("id > ?", offset).Limit(shardSize).Find(&users)

	childPool := pond.New(5, 0, pond.MinWorkers(5))
	var ciphers = make(chan Temp, len(users))
	var temps []Temp
	for _, user := range users {
		n := user
		childPool.Submit(func() {
			cipherText := aesgcm(n.No_id, key)
			cipherString := base64.StdEncoding.EncodeToString(cipherText)
			data := Temp{Id: n.ID, Encrypted_no_id: cipherString}
			ciphers <- data
		})
	}
	for i := 0; i < len(users); i++ {
		temps = append(temps, <-ciphers)
	}
	childPool.StopAndWait()
	db.Create(&temps)
}

func moveEncrypted(i int, db *gorm.DB, shardSize int) {
	rangeb := (i + 1) * shardSize
	rangea := rangeb - (shardSize - 1)

	db.Exec(`UPDATE users
SET    encrypted_no_id = (SELECT temps.encrypted_no_id
                 FROM   temps
                 WHERE  temps.id = users.id)
where id between ` + fmt.Sprint(rangea) + ` and ` + fmt.Sprint(rangeb) + `
and EXISTS (SELECT temps.encrypted_no_id
                 FROM   temps
                 WHERE  temps.id = users.id)`)
}

func errorHandler(message error) {
	if message != nil {
		fmt.Println("error :", message)
		panic(message)
	}
}

func pondProcess(db *gorm.DB, workers int, shardSize int) {
	pool := pond.New(workers, 10000, pond.MinWorkers(workers))

	// generate key for encrypt
	key := make([]byte, 32)
	_, err := rand.Read(key)
	errorHandler(err)

	count := int64(0)
	db.Table("users").Count(&count)
	iterate := float64(count) / float64(shardSize)
	if iterate-math.Round(iterate) > 0 {
		iterate = math.Round(iterate) + 1
	}

	ecnryptGroup := pool.Group()

	start := time.Now()

	for i := 0; i < int(iterate); i++ {
		n := i
		ecnryptGroup.Submit(func() {
			encrypt(n, db, key, shardSize)
		})
	}
	ecnryptGroup.Wait()

	for i := 0; i < int(iterate); i++ {
		n := i
		pool.Submit(func() {
			moveEncrypted(n, db, shardSize)
		})
	}

	pool.StopAndWait()

	duration := time.Since(start)
	fmt.Println(duration.Seconds())
}
