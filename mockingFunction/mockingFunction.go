package mockingfunction

var hello = func(name string) string {
	return "hello " + name
}

func hi(name string) string {
	return "hi " + name
}

func dialog() (result1 string, result2 string) {
	result1 = hello("roy")
	result2 = hi("agni")
	return
}
